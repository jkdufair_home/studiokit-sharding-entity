﻿using StudioKit.Sharding.Entity.Interfaces;
using StudioKit.Sharding.Models;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Sharding.Entity
{
	public class ShardDbContext<TConfiguration, TLicense>
		: DbContext, IShardDbContext<TConfiguration, TLicense>
		where TConfiguration : BaseShardConfiguration, new()
		where TLicense : BaseLicense, new()
	{
		public ShardDbContext() : this("DefaultConnection")
		{
		}

		public ShardDbContext(string nameOrConnectionString) : base(nameOrConnectionString)
		{
		}

		public virtual DbSet<TConfiguration> Configurations { get; set; }

		public virtual DbSet<TLicense> Licenses { get; set; }

		public TConfiguration GetConfiguration()
		{
			return Configurations.OrderByDescending(c => c.Id).FirstOrDefault();
		}

		public async Task<TConfiguration> GetConfigurationAsync(CancellationToken cancellationToken = default(CancellationToken))
		{
			return await Configurations.OrderByDescending(c => c.Id).FirstOrDefaultAsync(cancellationToken);
		}

		public TLicense GetLicense()
		{
			return Licenses.OrderByDescending(c => c.Id).FirstOrDefault();
		}

		public async Task<TLicense> GetLicenseAsync(CancellationToken cancellationToken = default(CancellationToken))
		{
			return await Licenses.OrderByDescending(c => c.Id).FirstOrDefaultAsync(cancellationToken);
		}
	}
}