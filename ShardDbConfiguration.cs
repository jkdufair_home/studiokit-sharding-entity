﻿using StudioKit.Data.Entity;

namespace StudioKit.Sharding.Entity
{
	public class ShardDbConfiguration : AzureDbConfiguration
	{
		public ShardDbConfiguration()
		{
			SetDefaultConnectionFactory(new ShardDbConnectionFactory());
		}
	}
}