using Microsoft.Azure.SqlDatabase.ElasticScale.ShardManagement;
using StudioKit.Sharding.Entity.Interfaces;
using StudioKit.Sharding.Extensions;
using StudioKit.Sharding.Models;
using StudioKit.Sharding.Utils;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Data.Entity.SqlServer;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace StudioKit.Sharding.Entity
{
	public class EntityShardManager<TContext, TConfiguration, TLicense> : ShardManager
		where TContext : DbContext, IShardDbContext<TConfiguration, TLicense>
		where TConfiguration : BaseShardConfiguration, new()
		where TLicense : BaseLicense, new()
	{
		protected virtual TContext GetDbContext(string connectionString)
		{
			throw new NotImplementedException();
		}

		protected virtual DbMigrationsConfiguration<TContext> GetDbMigrationsConfiguration()
		{
			throw new NotImplementedException();
		}

		public override void MigrateShardDatabase(ShardLocation location)
		{
			var configuration = GetDbMigrationsConfiguration();
			configuration.CommandTimeout = 120;
			configuration.TargetDatabase = new DbConnectionInfo(location.GetConnectionString(), SqlProviderServices.ProviderInvariantName);
			var migrator = new DbMigrator(configuration);
			var pendingMigrations = migrator.GetPendingMigrations().ToList();
			if (!pendingMigrations.Any())
			{
				ConsoleUtils.WriteInfo("No pending migrations for \"{0}\"", location.Database);
				return;
			}

			ConsoleUtils.WriteInfo("Migrating database \"{0}\"", location.Database);
			ConsoleUtils.WriteInfo("Applying pending migrations \"{0}\"", string.Join(", ", pendingMigrations));

			migrator.Update();

			ConsoleUtils.WriteInfo("Migration complete for \"{0}\"", location.Database);
		}

		public override void SeedShardDatabase(ShardLocation location)
		{
			var context = GetDbContext(location.GetConnectionString());
			SeedConfiguration(context);
			SeedLicense(context);
		}

		private void SeedConfiguration(TContext context)
		{
			var configuration = context.GetConfiguration() ?? new TConfiguration { Name = CustomerName };
			CustomerName = configuration.Name;

			ConsoleUtils.WriteInfo("Current Configuration: ");
			ConsoleUtils.WriteObjectProperties(configuration);
			Console.WriteLine();

			if (IsConsole)
			{
				ConsoleEditConfiguration(configuration);
			}

			if (configuration.Id == 0)
			{
				context.Configurations.Add(configuration);
			}

			if (context.ChangeTracker.HasChanges() || configuration.Id == 0)
			{
				context.SaveChanges();
				ConsoleUtils.WriteInfo("Saved Configuration: ");
				ConsoleUtils.WriteObjectProperties(configuration);
			}
			else
			{
				ConsoleUtils.WriteInfo("No changes were made.");
			}
			Console.WriteLine();
		}

		private void SeedLicense(TContext context)
		{
			var license = context.GetLicense();
			ConsoleUtils.WriteInfo("License: ");
			ConsoleUtils.WriteObjectProperties(license);
			Console.WriteLine();

			if (!IsConsole)
				return;

			Console.Write("Add a new License? (Y/N or leave blank to skip): ");
			var response = Console.ReadLine();
			if (string.IsNullOrWhiteSpace(response) || !response.ToUpperInvariant().Equals("Y"))
				return;

			license = new TLicense();
			ConsoleEditLicense(license);
			context.Licenses.Add(license);
			context.SaveChanges();
			ConsoleUtils.WriteInfo("Saved new License: ");
			ConsoleUtils.WriteObjectProperties(license);
			Console.WriteLine();
		}

		protected virtual void ConsoleEditConfiguration(TConfiguration configuration)
		{
			Console.Write("Add AllowedDomains as a comma separated string (or leave blank to skip): ");
			var response = Console.ReadLine();
			if (!string.IsNullOrWhiteSpace(response))
			{
				configuration.AllowedDomains = response;
			}

			Console.Write("Add a Logo Image? (Y/N or leave blank to skip): ");
			response = Console.ReadLine();
			if (!string.IsNullOrWhiteSpace(response) && response.ToUpperInvariant().Equals("Y"))
			{
				var dialog = new OpenFileDialog
				{
					Multiselect = false,
					Title = "Select Logo Image",
					Filter = "Image|*.png;*.gif;*.jpg;*.jpeg;*.bmp"
				};
				using (dialog)
				{
					if (dialog.ShowDialog() == DialogResult.OK)
					{
						var filename = dialog.FileName;
						configuration.Image = File.ReadAllBytes(filename);
					}
				}
			}

			Console.Write("Set the DefaultTimeZoneId (or leave blank to skip): ");
			response = Console.ReadLine();
			if (!string.IsNullOrWhiteSpace(response))
			{
				configuration.DefaultTimeZoneId = response;
			}

			Console.Write("Enable Caliper Analytics? (Y/N or leave blank to skip): ");
			response = Console.ReadLine();
			if (!string.IsNullOrWhiteSpace(response) && response.ToUpperInvariant().Equals("Y"))
			{
				configuration.CaliperEnabled = true;

				Console.Write("Set Caliper EventStore Hostname: ");
				response = Console.ReadLine();
				if (!string.IsNullOrWhiteSpace(response))
				{
					configuration.CaliperEventStoreHostname = response;
				}

				Console.Write("Set Caliper Person Namespace (e.g. https://purdue.edu/user/): ");
				response = Console.ReadLine();
				if (!string.IsNullOrWhiteSpace(response))
				{
					configuration.CaliperPersonNamespace = response;
				}
			}

			Console.Write("Set the Redis cache connection string (or leave blank to skip): ");
			response = Console.ReadLine();
			if (!string.IsNullOrWhiteSpace(response))
			{
				configuration.RedisConnectionString = response;
			}

			Console.Write("Should the Redis cache collect statistics? (Y/N or leave blank to skip): ");
			response = Console.ReadLine();
			if (!string.IsNullOrWhiteSpace(response) && response.ToUpperInvariant().Equals("Y"))
			{
				configuration.RedisShouldCollectStatistics = true;
			}

			Console.Write("Should the Instructor Sandbox be enabled? (Y/N or leave blank to skip): ");
			response = Console.ReadLine();
			if (!string.IsNullOrWhiteSpace(response) && response.ToUpperInvariant().Equals("Y"))
			{
				configuration.IsInstructorSandboxEnabled = true;

				var intResponse = ConsoleUtils.ReadIntegerInput("Set the Demo Expiration Day Limit (leave blank to skip): ", true);
				configuration.DemoExpirationDayLimit = intResponse;
			}
		}

		protected virtual void ConsoleEditLicense(TLicense license)
		{
			string response = null;
			DateTime startDate;
			while (string.IsNullOrWhiteSpace(response) || !DateTime.TryParse(response, out startDate))
			{
				Console.Write("Set License Start Date as \"mm/dd/yyyy\": ");
				response = Console.ReadLine();
				if (!string.IsNullOrWhiteSpace(response) && DateTime.TryParse(response, out startDate))
				{
					license.StartDate = startDate.ToUniversalTime();
				}
				else
				{
					ConsoleUtils.WriteWarning("You must enter a Start Date. Try again.");
				}
			}

			response = null;
			DateTime endDate;
			while (string.IsNullOrWhiteSpace(response) || !DateTime.TryParse(response, out endDate))
			{
				Console.Write("Set License End Date as \"mm/dd/yyyy\": ");
				response = Console.ReadLine();
				if (!string.IsNullOrWhiteSpace(response) && DateTime.TryParse(response, out endDate))
				{
					license.EndDate = endDate.ToUniversalTime();
				}
				else
				{
					ConsoleUtils.WriteWarning("You must enter a End Date. Try again.");
				}
			}
		}
	}
}