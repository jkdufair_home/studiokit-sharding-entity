using Microsoft.Azure.SqlDatabase.ElasticScale.ShardManagement;
using StudioKit.Sharding.Extensions;
using System;
using System.Data.Common;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;

namespace StudioKit.Sharding.Entity
{
	public class ShardDbConnectionFactory : IDbConnectionFactory
	{
		/// <summary>
		/// Creates a connection based on the given a shard key or connection string.
		/// </summary>
		/// <param name="shardKeyOrConnectionString">The shard key or connection string.</param>
		/// <returns>
		/// An initialized DbConnection.
		/// </returns>
		public DbConnection CreateConnection(string shardKeyOrConnectionString)
		{
			if (string.IsNullOrWhiteSpace(shardKeyOrConnectionString))
				throw new NullReferenceException(nameof(shardKeyOrConnectionString));

			if (shardKeyOrConnectionString.Equals(ShardDomainConstants.RootShardKey))
				throw new Exception("Cannot connect to database. Root shard key.");

			if (shardKeyOrConnectionString.Equals(ShardDomainConstants.InvalidShardKey))
				throw new Exception("Cannot connect to database. Invalid shard key.");

			// If provided param is a valid connection string, simply create a new connection
			try
			{
				var connectionBuilder = new SqlConnectionStringBuilder(shardKeyOrConnectionString);
				return new SqlConnectionFactory().CreateConnection(connectionBuilder.ConnectionString);
			}
			catch (ArgumentException)
			{
				//ignored - If "shardKeyOrConnectionString" is not a connection string, then proceed...
			}

			var shardMapManager = ShardManager.Instance.ShardMapManger;
			if (shardMapManager == null)
				throw new Exception("Shard Map Manager does not exist");

			var shardMap = ShardManager.Instance.DefaultShardMap;
			if (shardMap == null)
				throw new Exception("Shard Map does not exist");

			// Ask shard map to broker a validated connection for the given key
			return shardMap.OpenConnectionForKey(shardKeyOrConnectionString.ToPointMapping(),
				ShardConfiguration.GetCredentialsConnectionString(),
				ConnectionOptions.Validate);
		}
	}
}